Array.prototype.concatAll = function () {
  var result = [];
  this.forEach(function (subArray) {
    subArray.forEach(function (elem) {
      result.push(elem);
    })
  });

  return result;
};
