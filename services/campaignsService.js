MTCampaignsServices.factory('campaignsService', ['$http', '$q',
  function ($http, $q) {

    var getCampaignById = function (campaignId) {

      var defer = $q.defer();
      $http({
        method: 'GET',
        url: campaignsApiUrl + '/campaigns/' + campaignId
      })
        .success(function (data) {
          defer.resolve(data);
        }).
        error(function (error) {
          defer.reject(error);
        });
      return defer.promise;
    };

    var getAllCampaigns = function () {


      var defer = $q.defer();

      $http({
        url: campaignsApiUrl + '/campaigns',
        method: 'GET'
      })
        .success(function (data) {

          defer.resolve(data);
        }).
        error(function (error) {
          defer.reject(error);
        });
      return defer.promise;
    };

    var setDefaultValuesIfEmpty = function (data) {
      return data.map(function (campReport) {
        campReport.epc = campReport.epc || 0;
        campReport.revenue = campReport.revenue || 0;
        campReport.pnl = campReport.pnl || 0;
        campReport.campaignClick = campReport.campaignClick || 0;
        campReport.offerClick = campReport.offerClick || 0;
        campReport.spend = campReport.spend || 0;
        campReport.lpClick = campReport.lpClick || 0;

        campReport.os = campReport.os || 'unknown';
        campReport.browser = campReport.browser || 'unknown';
        campReport.brandName = campReport.brandName || 'unknown';
        campReport.modelName = campReport.modelName || 'unknown';
        campReport.marketingName = campReport.marketingName || 'unknown';
        campReport.resolution = campReport.resolution || 'unknown';
        campReport.manufacturer = campReport.manufacturer || 'unknown';

        return campReport;
      });
    };

    return {
      getCampaignById: getCampaignById,
      getAllCampaigns: getAllCampaigns,
      setDefaultValues: setDefaultValuesIfEmpty
    }
  }
]);