MTCampaignsServices.factory('fieldHeadersService', [function () {

  function HeadersBuilder() {
    this.dimensionHeaders = [];
    this.metricsHeaders = [];
    if (!(this instanceof HeadersBuilder)) return new HeadersBuilder();
  }

  HeadersBuilder.prototype.addDimensionHeader = function (dimensionHeader) {
    this.dimensionHeaders.push(dimensionHeader);
    return this;
  };

  HeadersBuilder.prototype.getHeaders = function () {
    return this.dimensionHeaders.concat(this.metricsHeaders);
  };

  HeadersBuilder.prototype.addDefaultHeadersForOffers = function () {
    this.dimensionHeaders = ["offerId"];
    this.metricsHeaders = ["offerClick", "campaignClick", "lpClick", "pnl", "epc", "spend", "revenue"];
    return this;
  };

  HeadersBuilder.prototype.addDefaultHeadersForCampaign = function () {
    this.dimensionHeaders = ["campId"];
    this.metricsHeaders = ["epc", "pnl", "campaignClick", "offerClick", "lpClick", "revenue", "spend"];
    return this;
  };

  HeadersBuilder.prototype.addDefaultHeadersForDataCampaignStats = function () {
    this.dimensionHeaders = [];
    this.metricsHeaders = ["epc", "pnl", "campaignClick", "offerClick", "lpClick", "revenue", "spend"];
    return this;
  };

  return {
    hb: HeadersBuilder
  };

}]);
