
MTCampaignsServices.factory('authorized', ['$cookies', function ($cookies) {

    var authorized = function () {

        if (!$cookies.get('token')) {
            return false;
        }
        else {
            return true;
        }
    };
    return authorized;
}]);