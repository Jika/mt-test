MTCampaignsServices.factory('reportQueryBuilderService', [function () {


  function QueryBuilder() {
    this.dimensions = [];
    this.metrics = ["revenue", "spend", "epc", "pnl", "campaignClick", "offerClick"];
    this.filters = [];
    this.date = null;

    if (!(this instanceof QueryBuilder)) return new QueryBuilder();
  }

  QueryBuilder.prototype.addDimension = function (dimension) {
    if (dimension && this.dimensions.indexOf(dimension) === -1) this.dimensions.push(dimension);
    return this;
  };

  QueryBuilder.prototype.addMetric = function (metric) {
    if (metric && this.metrics.indexOf(metric) === -1) this.metrics.push(metric);
    return this;
  };

  QueryBuilder.prototype.addDimensions = function (dimensions) {
    dimensions.map(this.addDimension.bind(this));
    return this;
  };

  QueryBuilder.prototype.addMetrics = function (metrics) {
    metrics.map(this.addMetric.bind(this));
    return this;
  };

  QueryBuilder.prototype.getQuery = function () {
    var query = {
      columns: this.metrics.reverse().concat(this.dimensions.reverse()),
      filter: this.filters
    };

    if (this.date) query['date'] = this.date;

    return query;
  };

  QueryBuilder.prototype.addFilter = function (filter) {
    if (filter) this.filters.push(filter);
    return this;
  };

  QueryBuilder.prototype.addDateFilter = function (start, end, timezone) {
    if (start && end && timezone) this.date = {start: start, end: end, timezone: timezone};
    return this;
  };

  QueryBuilder.prototype.addCampaignFilter = function (campId) {
    if (campId) {
      this.filters.push({
        field: 'campId',
        value: campId
      });
    }
    return this;
  };

  return {
    qb: QueryBuilder
  }
}]);
