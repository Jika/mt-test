MTCampaignsServices.factory('reportFilterService', [function () {

  var getFilter = function (campId) {
    return [{
      field: 'campId',
      value: campId
    }];
  };

  var addFilter = function (campId) {
    return campId ? getFilter(campId) : [];
  };

  return {
    addFilter: addFilter
  };
}]);
