MTCampaignsServices.factory('reportRequirementsService', [
  function () {

    var offersColumns = function () {
      return ["revenue", "spend", "epc", "pnl", "lpClick", "campaignClick", "offerClick", 'offerId', 'campId'];
    };

    var campaignColumns = function () {
      return ["spend", "revenue", "lpClick", "offerClick", "campaignClick", "pnl", "epc", "campId"];
    };

    return {
      offersColumns: offersColumns,
      campaignColumns: campaignColumns
    }
  }
]);
