var metrics = ["spend", "revenue", "pnl", "epc"];

function roundDataInObj(data) {
  return metrics.reduce(function (resultData, metric) {
    if (resultData[metric]) resultData[metric] = parseFloat(resultData[metric]).toFixed(2);
    return resultData;
  }, data)
}
function roundReport(data){
    if(Object.prototype.toString.call(data) !== "[object Array]") return roundDataInObj(data);
    return data.map(function(obj){
      return roundDataInObj(obj);
    })
}

MTCampaignsServices.factory('reportsService', ['$http', 'MTReportingServer', '$q', 'reportRequirementsService',
    function ($http, MTReportingServer, $q, reportRequirementsService) {

        var getCampaignReport = function (reportRequirements) {

            var defer = $q.defer();

            $http({
                method: 'POST',
                url: MTReportingServer + '/reporting/',
                data: reportRequirements
            })
                .success(function (data) {
                    var reportName = data.reportName;
                    $http({
                        method: 'GET',
                        url: MTReportingServer + '/reporting/' + reportName + '?' + jQuery.param({
                            'limit': offersPerPage
                        })
                    })
                        .success(function (data) {

                            var offers = {
                                data: roundReport(data),
                                reportName: reportName
                            }
                            defer.resolve(offers);
                        }).
                        error(function (error) {
                            defer.reject(error);
                        });
                }).
                error(function (error) {
                    defer.reject(error);
                });
            return defer.promise;
        }

        var getOffersReport = function (reportRequirements) {

            var defer = $q.defer();

            $http({
                method: 'POST',
                url: MTReportingServer + '/reporting/',
                data: reportRequirements
            })
                .success(function (data) {
                    var totalCount = data.totalCount;
                    var reportName = data.reportName;
                    $http({
                        url: MTReportingServer + '/reporting/' + reportName + '?' + jQuery.param({
                            'limit': offersPerPage
                        }),
                        method: 'GET'
                    })
                        .success(function (data) {
                            var offers = {
                                data: roundReport(data),
                                totalCount: totalCount,
                                reportName: reportName
                            };
                            defer.resolve(offers);
                        }).
                        error(function (error) {
                            defer.reject(error);
                        });
                }).
                error(function (error) {
                    defer.reject(error);
                });
            return defer.promise;
        }

        var getOffersSet = function (reportName, skip) {

            var defer = $q.defer();

            $http({
                url: MTReportingServer + '/reporting/' + reportName + '?' + jQuery.param({
                    'skip': skip,
                    'limit': offersPerPage
                }),
                method: 'GET'
            })
                .success(function (data) {
                    var offers = [];
                    offers['data'] = roundReport(data);
                    defer.resolve(offers);
                }).
                error(function (error) {
                    defer.reject(error);
                });

            return defer.promise;
        }
        return {
            getOffersReport: getOffersReport,
            getCampaignReport: getCampaignReport,
            getOffersSet: getOffersSet
        }
    }
]);