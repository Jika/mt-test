MTCampaignsServices.factory('campaignTypesService', [
    function () {

        var campaignTypes = [
            {
                'type': 'directLinking',
                'label': 'Direct linking'
            },
            {
                'type': 'landingPage',
                'label': 'Landing Page'
            },
            {
                'type': 'path',
                'label': 'Path'
            }
        ];
        var pathTypeCampaignTypes = [
            {
                type: 'lp',
                label: 'Lp Click'
            },
            {
                type: 'path',
                label: 'Path Click'
            }
        ]
        return {
            campaignTypes: campaignTypes,
            pathTypeCampaignTypes: pathTypeCampaignTypes
        }
    }
]);