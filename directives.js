'use strict';

/* Controllers */


var mtCampaignsDirectives = angular.module('mtCampaignsDirectives', []);

mtCampaignsDirectives.directive('campaignsListTable', function () {
  return {
    templateUrl: 'directives/сampaignsListTable.html'
  };
});

mtCampaignsDirectives.directive('campaignOffersBlock', function () {
  return {
    templateUrl: 'directives/campaignOffersBlock.html'
  };
});

mtCampaignsDirectives.directive('campaignTokensBlock', function () {
  return {
    templateUrl: 'directives/campaignTokensBlock.html'
  };
});

mtCampaignsDirectives.directive('campaignLandingPagesBlock', function () {
  return {
    templateUrl: 'directives/campaignLandingPagesBlock.html'
  };
});

mtCampaignsDirectives.directive('campaignPathBlock', function () {
  return {
    templateUrl: 'directives/campaignPathBlock.html'
  };
});

mtCampaignsDirectives.directive('menu', function () {
  return {
    restrict: "C",
    transclude: true,
    templateUrl: 'directives/menu.html'
  };
});

mtCampaignsDirectives.directive('timeZones', function () {
  return {
    templateUrl: 'directives/timeZones.html'
  };
});

mtCampaignsDirectives.directive('campaignOverview', function () {
  return {
    templateUrl: 'directives/campaignOverview.html',
    controller: 'campaignStatsCampaignOverview'
  };
});

mtCampaignsDirectives.directive('campaignStats', function () {
  return {
    templateUrl: 'directives/campaignStats.html',
    contoller: 'campaignStatsCampaignStatsController'

  };
});

mtCampaignsDirectives.directive('offerOverview', function () {
  return {
    templateUrl: 'directives/offerOverview.html',
    controller: 'campaignStatsOfferOverviewController'
  };
});


mtCampaignsDirectives.directive('campaignStatsFilter', function () {
  return {
    templateUrl: 'directives/campaignStatsFilter.html',
    controller: 'campaignStatsFilterController'
  };
});

mtCampaignsDirectives.directive('fieldData', function () {
  return {
    restrict: 'E',
    templateUrl: 'directives/fieldData.html',
    scope:{
      object: '=obj',
      fieldName: '=field',
      campaignNames: '=',
      offerNames: '='
    }
  };
});

mtCampaignsDirectives.directive('fieldHeader', function () {
  return {
    restrict: 'E',
    templateUrl: 'directives/fieldHeader.html',
    scope:{
      object: '=obj',
      fieldName: '=field',
      campaignNames: '='
    }
  };
});

mtCampaignsDirectives.directive('title', function () {
  return {
    restrict: 'E',
    templateUrl: 'directives/titleBar.html',
    controller: 'titleBarController'
  };
});

mtCampaignsDirectives.directive('campaignLinkBlock', function () {
  return {
    templateUrl: 'directives/campaignLinkBlock.html',
    controller: 'campaignTrackingUrlController'
  };
});


mtCampaignsDirectives.directive('campaignConversionLinkBlock', function () {
  return {
    templateUrl: 'directives/campaignConversionLinkBlock.html',
    controller: 'campaignConversionUrlController'
  };
});