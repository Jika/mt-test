'use strict';

/* mtCampaigns Module */

var mtCampaignsApp = angular.module('mtCampaignsApp', [
  'ngRoute',
  'mtCampaignsControllers',
  'mtCampaignsDirectives',
  'MTCampaign.services',
  'ngCookies',
  'brantwills.paging',
  'zeroclipboard'
]).run(function ($rootScope, $location) {
  $rootScope.location = $location;

  $rootScope.$on('$viewContentLoaded', function () {
    App.init();
    App.dashBoard();


  });
});

mtCampaignsApp.config(['$routeProvider', 'uiZeroclipConfigProvider',
  function ($routeProvider, uiZeroclipConfigProvider) {
    $routeProvider.
      when('/campaigns_list', {
        templateUrl: 'templates/campaignsList.html',
        controller: 'listCampaignsController'
      }).
      when('/add_campaign/:campaign_id?', {
        templateUrl: 'templates/addCampaign.html',
        controller: 'addCampaignController'
      }).
      when('/edit_campaign/:campaign_id', {
        templateUrl: 'templates/editCampaign.html',
        controller: 'editCampaignController'
      }).
      when('/reporting/:campaign_id?', {
        templateUrl: 'templates/report.html',
        controller: 'campaignReportController'
      }).
      when('/authentification', {
        templateUrl: 'templates/authentification.html',
        controller: 'authController'
      }).
      otherwise({
        redirectTo: '/campaigns_list'
      });


    uiZeroclipConfigProvider.setZcConf({
      swfPath: 'bower_components/zeroclipboard/dist/ZeroClipboard.swf'
    });

  }]);

var mtCampaignsControllers = angular.module('mtCampaignsControllers', []);

var MTCampaignsServices = angular.module('MTCampaign.services', []);

mtCampaignsApp.constant('serverUrl', campaignsApiUrl);
mtCampaignsApp.constant('MTReportingServer', reportingApiUrl);
mtCampaignsApp.constant('offersPerPage', offersPerPage);
mtCampaignsApp.constant('deliveryTrackingUrl', deliveryTrackingUrl);
